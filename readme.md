# Simple Recruiting Process
- This application provides a simple way of controlling a recruiting process

## How to run it? It is simple!
- <code>cd *'PROJECT_FOLDER'*</code>.
- <code>chmod 777 mvnw</code> (if the file doesn't have permission)
- <code>./mvnw clean package</code>
- <code>java -jar target/recruiting-process-0.0.1-SNAPSHOT.jar</code>
- Done! The application is running on the port :8080
- While running you can check the API Documentation at: <http://localhost:8080/swagger-ui.html>

## How to test it? It is even simpler!
- <code>cd *'PROJECT_FOLDER'*</code>.
- <code>./mvnw clean test</code>.
- These two commands will run the tests

### Code Quality
- Check the code quality at <https://sonarcloud.io/dashboard?id=recruiting-process>

#### TODOs
- Since the code creates somehow a CRUD framework, that should be documented
- Add Deploy to gitlab pipeline
- Gitlab badges