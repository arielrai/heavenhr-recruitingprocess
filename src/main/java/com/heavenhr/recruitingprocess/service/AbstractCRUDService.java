package com.heavenhr.recruitingprocess.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.heavenhr.recruitingprocess.dto.BaseDTO;
import com.heavenhr.recruitingprocess.model.BaseEntity;

import javassist.NotFoundException;

public abstract class AbstractCRUDService<E extends BaseEntity<D>, D extends BaseDTO, R extends JpaRepository<E, Long>> {

	@Autowired
	protected R repository;
	
	@Transactional
	public D save(D dto) {
		E newEntity = newEntity();
		newEntity.fromDTO(dto);
		return enhanceDTO(repository.save(newEntity).toDTO());
	}

	protected abstract E newEntity();
	
	public D findOne(Long id) throws NotFoundException {
		Optional<E> optionalEntity = repository.findById(id);
		if (optionalEntity.isPresent()) {
			return optionalEntity.get().toDTO();
		}
		throw new NotFoundException(String.format("%s with id %s not found", entityName(), id));
	}
	
	public List<D> findAll(Pageable pageable) {
		return repository.findAll(pageable).stream()
				.map(o -> enhanceDTO(o.toDTO())).collect(Collectors.toList());
	}

	protected abstract String entityName();
	
	protected D enhanceDTO(D dto) {
		//@override in case you need to enhance your DTO
		return dto;
	}
}
