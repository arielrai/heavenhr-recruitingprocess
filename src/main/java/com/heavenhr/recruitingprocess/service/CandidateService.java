package com.heavenhr.recruitingprocess.service;

import org.springframework.stereotype.Service;

import com.heavenhr.recruitingprocess.dto.CandidateDTO;
import com.heavenhr.recruitingprocess.model.Candidate;
import com.heavenhr.recruitingprocess.repository.CandidateRepository;

@Service
public class CandidateService extends AbstractCRUDService<Candidate, CandidateDTO, CandidateRepository> {

	@Override
	protected Candidate newEntity() {
		return new Candidate();
	}

	@Override
	protected String entityName() {
		return "Candidate";
	}

}
