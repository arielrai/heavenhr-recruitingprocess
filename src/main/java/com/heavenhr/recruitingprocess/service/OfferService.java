package com.heavenhr.recruitingprocess.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.heavenhr.recruitingprocess.dto.OfferDTO;
import com.heavenhr.recruitingprocess.model.Offer;
import com.heavenhr.recruitingprocess.repository.ApplicationRepository;
import com.heavenhr.recruitingprocess.repository.OfferRepository;

@Service
public class OfferService extends AbstractCRUDService<Offer, OfferDTO, OfferRepository> {

	@Autowired
	private ApplicationRepository applicationRepository;
	
	@Override
	protected Offer newEntity() {
		return new Offer();
	}

	@Override
	protected String entityName() {
		return "Offer";
	}
	
	@Override
	protected OfferDTO enhanceDTO(OfferDTO dto) {
		dto.setNumberOfApplicants(applicationRepository.countByOfferId(dto.getId()));
		return dto;
	}
}
