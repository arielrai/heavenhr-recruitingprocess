package com.heavenhr.recruitingprocess.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;
import com.heavenhr.recruitingprocess.model.Application;
import com.heavenhr.recruitingprocess.model.ApplicationStatus;
import com.heavenhr.recruitingprocess.model.Candidate;
import com.heavenhr.recruitingprocess.model.Offer;
import com.heavenhr.recruitingprocess.notification.ApplicationStatusChangeNotification;
import com.heavenhr.recruitingprocess.repository.ApplicationRepository;
import com.heavenhr.recruitingprocess.repository.CandidateRepository;
import com.heavenhr.recruitingprocess.repository.OfferRepository;

import javassist.NotFoundException;

@Service
public class ApplicationService {

	@Autowired
	private OfferRepository offerRepository;
	@Autowired
	private CandidateRepository candidateRepository;
	@Autowired
	private ApplicationRepository repository;
	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Transactional
	public ApplicationDTO applyToOffer(Long id, Long candidateId) throws NotFoundException {
		Optional<Offer> offerOptional = offerRepository.findById(id);

		StringBuilder sb = new StringBuilder();
		boolean offerNotPresent = !offerOptional.isPresent();
		if (offerNotPresent) {
			sb.append(id).append("is not a valid Offer id");
		}

		Optional<Candidate> candidateOptional = candidateRepository.findById(candidateId);
		boolean candidateNotPresent = !candidateOptional.isPresent();
		if (candidateNotPresent) {
			if (sb.length() > 0) {
				sb.append(" and ");
			}
			sb.append(candidateId).append("is not a valid Candidate id");
		}
		if (candidateNotPresent || offerNotPresent) {
			throw new NotFoundException(sb.toString());
		} else {
			return repository.save(new Application(offerOptional.get(), candidateOptional.get(), ApplicationStatus.APPLIED)).toDTO();
		}

	}

	@Transactional
	public List<ApplicationDTO> findAllApplicantsByOfferId(Long offerId, Pageable pageable) {
		return repository.findAllByOfferId(offerId, pageable).stream().map(a -> a.toDTO()).collect(Collectors.toList());
	}

	@Transactional
	public void updateStatus(Long id, String status) throws NotFoundException {
		Optional<Application> applicationOptional = repository.findById(id);
		if (!applicationOptional.isPresent()) {
			throw new NotFoundException(String.format("Application with id: %s not found", id));
		}
		ApplicationStatus newStatus = ApplicationStatus.valueOf(status.toUpperCase());
		Application application = applicationOptional.get();
		//Will be only executed if the transaction is completed successfully 
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
			@Override
			public void afterCommit() {
				eventPublisher.publishEvent(new ApplicationStatusChangeNotification(application.toDTO(), newStatus));
			}
		});
		
		application.setStatus(newStatus);
		repository.save(application);
	}

}
