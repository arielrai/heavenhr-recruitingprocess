package com.heavenhr.recruitingprocess.dto;

import lombok.Data;

@Data
public class CandidateDTO extends BaseDTO {

	private String firstName;
	private String lastName;
	private String email; 
	private String resumeText;
}
