package com.heavenhr.recruitingprocess.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class OfferDTO extends BaseDTO {
	
	private String jobTitle;
	private LocalDate startDate;
	private long numberOfApplicants;
}
