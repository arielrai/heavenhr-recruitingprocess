package com.heavenhr.recruitingprocess.dto;

import com.heavenhr.recruitingprocess.model.ApplicationStatus;

import lombok.Data;

@Data
public class ApplicationDTO extends BaseDTO{

	private OfferDTO offer;
	private ApplicationStatus status;
	private CandidateDTO candidate;
}
