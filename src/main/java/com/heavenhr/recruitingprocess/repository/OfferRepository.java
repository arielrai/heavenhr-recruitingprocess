package com.heavenhr.recruitingprocess.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.heavenhr.recruitingprocess.model.Offer;

public interface OfferRepository extends JpaRepository<Offer, Long> {

}
