package com.heavenhr.recruitingprocess.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.heavenhr.recruitingprocess.model.Candidate;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {

}
