package com.heavenhr.recruitingprocess.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.heavenhr.recruitingprocess.model.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long> {
	
	/**
	 * this method was added here in order to count the elements more efficiently
	 * this way we don't need to load the entire list to count the applications of an Offer
	 * @param offer - the offer we want to count the number of applications
	 * @return the number applications for the offer 
	 */
	Long countByOfferId(Long offerId);
	
	List<Application> findAllByOfferId(Long id, Pageable pageable);
}
