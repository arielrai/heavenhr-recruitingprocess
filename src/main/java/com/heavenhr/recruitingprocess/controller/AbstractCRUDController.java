package com.heavenhr.recruitingprocess.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.heavenhr.recruitingprocess.dto.BaseDTO;
import com.heavenhr.recruitingprocess.model.BaseEntity;
import com.heavenhr.recruitingprocess.service.AbstractCRUDService;
import com.heavenhr.recruitingprocess.service.URLBuilderService;

import javassist.NotFoundException;

public class AbstractCRUDController<D extends BaseDTO, E extends BaseEntity<D>, S extends AbstractCRUDService<E, D, ? extends JpaRepository<E, Long>>> {

	@Autowired
	protected S service;
	@Autowired
	protected URLBuilderService urlBuilderService;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public URI add(@RequestBody D offerDTO) {
		D dto = service.save(offerDTO);
		return urlBuilderService.buildCreateURL(dto.getId());
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public D get(@PathVariable("id")Long id) throws NotFoundException {
		return service.findOne(id);
	}

	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<D> getOffers(Pageable pageable) {
		return service.findAll(pageable);
	}
}
