package com.heavenhr.recruitingprocess.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;
import com.heavenhr.recruitingprocess.dto.OfferDTO;
import com.heavenhr.recruitingprocess.model.Offer;
import com.heavenhr.recruitingprocess.service.ApplicationService;
import com.heavenhr.recruitingprocess.service.OfferService;

import javassist.NotFoundException;

@RestController
@RequestMapping("offers")
public class OfferController extends AbstractCRUDController<OfferDTO, Offer, OfferService> {
	
	@Autowired
	private ApplicationService applicationService;
	
	@GetMapping("/{id}/applications")
	@ResponseStatus(code = HttpStatus.OK)
	public List<ApplicationDTO> getApplications(@PathVariable("id")Long id, Pageable pageable) throws NotFoundException {
		return applicationService.findAllApplicantsByOfferId(id, pageable);
	}
	
	@PostMapping("/{id}/apply/{candidateId}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ApplicationDTO apply(@PathVariable("id")Long id, @PathVariable("candidateId")Long candidateId) throws NotFoundException {
		return applicationService.applyToOffer(id, candidateId);
	}
}
