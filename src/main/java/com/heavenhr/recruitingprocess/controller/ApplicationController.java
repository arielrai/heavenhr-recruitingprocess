package com.heavenhr.recruitingprocess.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.heavenhr.recruitingprocess.service.ApplicationService;

import javassist.NotFoundException;

@RestController
@RequestMapping("applications")
public class ApplicationController {

	@Autowired
	private ApplicationService applicationService;
	
	@PostMapping("{applicationId}/changeStatus/{status}")
	public void changeApplicationStatus(@PathVariable("applicationId") Long id, @PathVariable("status") String status) throws NotFoundException {
		applicationService.updateStatus(id, status);
	}
}
