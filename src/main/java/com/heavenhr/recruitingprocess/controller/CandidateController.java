package com.heavenhr.recruitingprocess.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.heavenhr.recruitingprocess.dto.CandidateDTO;
import com.heavenhr.recruitingprocess.model.Candidate;
import com.heavenhr.recruitingprocess.service.CandidateService;

@RestController
@RequestMapping("candidates")
public class CandidateController extends AbstractCRUDController<CandidateDTO, Candidate, CandidateService>{

}
