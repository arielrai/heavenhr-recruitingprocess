package com.heavenhr.recruitingprocess.notification;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;
import com.heavenhr.recruitingprocess.model.ApplicationStatus;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApplicationStatusChangeNotification {
	private ApplicationDTO applicationDTO;
	private ApplicationStatus newStatus;
}