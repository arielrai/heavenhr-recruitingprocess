package com.heavenhr.recruitingprocess.notification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;
import com.heavenhr.recruitingprocess.dto.CandidateDTO;

@Component
public class ApplicationStatusNotificationManager {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationStatusNotificationManager.class);

	@EventListener(condition = "#statusChange.newStatus.name() == 'HIRED'", classes = ApplicationStatusChangeNotification.class)
	public void notifyHiredCandidates(ApplicationStatusChangeNotification statusChange) {
		ApplicationDTO application = statusChange.getApplicationDTO();
		CandidateDTO candidate = application.getCandidate();
		logger.info("Candidate {} {} hired as {}", candidate.getFirstName(), candidate.getLastName(),
				application.getOffer().getJobTitle());
	}

	@EventListener(condition = "#statusChange.newStatus.name() == 'REJECTED'", classes = ApplicationStatusChangeNotification.class)
	public void notifyRejectedCandidates(ApplicationStatusChangeNotification statusChange) {
		ApplicationDTO application = statusChange.getApplicationDTO();
		CandidateDTO candidate = application.getCandidate();
		logger.info("Candidate {} {} rejected for {}", candidate.getFirstName(), candidate.getLastName(),
				application.getOffer().getJobTitle());
	}

	@EventListener(condition = "#statusChange.newStatus.name() == 'INVITED'", classes = ApplicationStatusChangeNotification.class)
	public void notifyInvitedCandidates(ApplicationStatusChangeNotification statusChange) {
		ApplicationDTO application = statusChange.getApplicationDTO();
		CandidateDTO candidate = application.getCandidate();
		logger.info("Candidate {} {} invited to {}", candidate.getFirstName(), candidate.getLastName(),
				application.getOffer().getJobTitle());
	}
}
