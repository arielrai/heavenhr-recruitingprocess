package com.heavenhr.recruitingprocess.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.heavenhr.recruitingprocess.dto.OfferDTO;

import lombok.Data;

@Data
@Entity
public class Offer extends BaseEntity<OfferDTO> {

	@Column(unique = true, nullable = false)
	private String jobTitle;
	
	@Column(nullable = false)
	private LocalDate startDate = LocalDate.now();
	
	@OneToMany(mappedBy = "offer", cascade = CascadeType.ALL)
    private List<Application> applications;

	@Override
	public OfferDTO toDTO() {
		OfferDTO offerDTO = new OfferDTO();
		offerDTO.setJobTitle(this.getJobTitle());
		offerDTO.setStartDate(this.getStartDate());
		offerDTO.setId(this.getId());
		return offerDTO;
	}

	@Override
	public void fromDTO(OfferDTO dto) {
		this.setJobTitle(dto.getJobTitle());
		this.setStartDate(dto.getStartDate());
	}
	
}
