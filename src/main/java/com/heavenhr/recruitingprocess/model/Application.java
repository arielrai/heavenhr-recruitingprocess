package com.heavenhr.recruitingprocess.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"offer_id", "candidate_id"})})
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Application extends BaseEntity<ApplicationDTO> {

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "offer_id", nullable = false)
	private Offer offer;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "candidate_id", nullable = false)
	private Candidate candidate;
	
	@Enumerated(EnumType.STRING)
	private ApplicationStatus status = ApplicationStatus.APPLIED;

	@Override
	public ApplicationDTO toDTO() {
		ApplicationDTO applicationDTO = new ApplicationDTO();
		applicationDTO.setOffer(this.offer.toDTO());
		applicationDTO.setCandidate(this.candidate.toDTO());
		applicationDTO.setStatus(this.getStatus());
		applicationDTO.setId(this.getId());
		return applicationDTO;
	}

	@Override
	public void fromDTO(ApplicationDTO dto) {
		throw new UnsupportedOperationException();
	}
	
}
