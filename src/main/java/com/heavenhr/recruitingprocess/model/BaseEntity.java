package com.heavenhr.recruitingprocess.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.heavenhr.recruitingprocess.dto.BaseDTO;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class BaseEntity<D extends BaseDTO> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public abstract D toDTO();
	
	public abstract void fromDTO(D dto);
}
