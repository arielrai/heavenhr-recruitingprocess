package com.heavenhr.recruitingprocess.model;

public enum ApplicationStatus {

	APPLIED, INVITED, REJECTED, HIRED
	
}
