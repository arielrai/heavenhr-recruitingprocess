package com.heavenhr.recruitingprocess.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.heavenhr.recruitingprocess.dto.CandidateDTO;

import lombok.Data;

@Data
@Entity
public class Candidate extends BaseEntity<CandidateDTO> {

	@NotBlank
	@Column(nullable = false)
	private String firstName;

	@NotBlank
	@Column(nullable = false)
	private String lastName;
	
	@Email
	@Column(nullable = false)
	private String email; 
	
	@NotBlank
	@Column(length = 4000, nullable = false)
	private String resumeText;

	@Override
	public CandidateDTO toDTO() {
		CandidateDTO candidateDTO = new CandidateDTO();
		candidateDTO.setFirstName(this.getFirstName());
		candidateDTO.setLastName(this.getLastName());
		candidateDTO.setResumeText(this.getResumeText());
		candidateDTO.setEmail(this.getEmail());
		candidateDTO.setId(this.getId());
		return candidateDTO;
	}

	@Override
	public void fromDTO(CandidateDTO dto) {
		this.setFirstName(dto.getFirstName());
		this.setLastName(dto.getLastName());
		this.setResumeText(dto.getResumeText());
		this.setEmail(dto.getEmail());
	}
	
}
