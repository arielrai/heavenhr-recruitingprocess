package com.heavenhr.recruitingprocess.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class RequestUtils {

	public static HttpEntity<String> buildJsonRequest(String json) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return new HttpEntity<String>(json, headers);
	}
}
