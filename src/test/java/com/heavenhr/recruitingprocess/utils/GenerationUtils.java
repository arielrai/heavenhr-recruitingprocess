package com.heavenhr.recruitingprocess.utils;

import java.security.SecureRandom;
import java.time.LocalDate;

import com.heavenhr.recruitingprocess.dto.CandidateDTO;
import com.heavenhr.recruitingprocess.dto.OfferDTO;

public class GenerationUtils {

	private static final String ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";
	private static final SecureRandom RANDOM = new SecureRandom();
	
	public static CandidateDTO generateRandomCandidateDTO() {
		CandidateDTO candidateDTO = new CandidateDTO();
		candidateDTO.setFirstName(generate(6));
		candidateDTO.setLastName(generate(8));
		candidateDTO.setResumeText(String.format("CURRICULUM VITAE \n%S", generate(100)));
		candidateDTO.setEmail(String.format("%s@random.com", generate(10)));
		return candidateDTO;
	}

	public static OfferDTO generateRandomOfferDTO() {
		OfferDTO offerDTO = new OfferDTO();
		offerDTO.setJobTitle(String.format("Java %s Developer", generate(10)));
		offerDTO.setStartDate(LocalDate.now());
		return offerDTO;
	}
	
    /**
     * Generates random string of given length from Base65 alphabet (numbers, lowercase letters, uppercase letters).
     *
     * @param count length
     * @return random string of given length
     */
    private static String generate(int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; ++i) {
            sb.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return sb.toString();
    }

}
