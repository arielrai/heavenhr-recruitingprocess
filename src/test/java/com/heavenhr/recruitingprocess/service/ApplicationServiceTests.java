package com.heavenhr.recruitingprocess.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;
import com.heavenhr.recruitingprocess.dto.CandidateDTO;
import com.heavenhr.recruitingprocess.dto.OfferDTO;
import com.heavenhr.recruitingprocess.model.ApplicationStatus;
import com.heavenhr.recruitingprocess.repository.ApplicationRepository;
import com.heavenhr.recruitingprocess.repository.CandidateRepository;
import com.heavenhr.recruitingprocess.repository.OfferRepository;
import com.heavenhr.recruitingprocess.utils.GenerationUtils;

import javassist.NotFoundException;

@SpringBootTest
public class ApplicationServiceTests {

	@Autowired
	private OfferService offerService;
	@Autowired
	private CandidateService candidateService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private ApplicationRepository applicationRepository;
	@Autowired
	private CandidateRepository candidateRepository;
	@Autowired
	private OfferRepository offerRepository;

	@AfterEach
	public void cleanUp() {
		applicationRepository.deleteAll();
		candidateRepository.deleteAll();
		offerRepository.deleteAll();
	}

	@BeforeEach
	public void setUp() {
		for (int i = 0; i < 5; i++) {
			offerService.save(GenerationUtils.generateRandomOfferDTO());
		}

		for (int i = 0; i < 5; i++) {
			candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		}
	}

	@Test
	public void shouldExpectRightNumberOfApplicants() throws NotFoundException {
		List<OfferDTO> offers = offerService.findAll(Pageable.unpaged());
		List<CandidateDTO> candidates = candidateService.findAll(Pageable.unpaged());

		for (OfferDTO offer : offers) {
			for (CandidateDTO candidateDTO : candidates) {
				applicationService.applyToOffer(offer.getId(), candidateDTO.getId());
			}
			candidates.remove(0);
		}

		// refresh offers to get number of applicants
		offers = offerService.findAll(Pageable.unpaged());
		int applicants = 5;
		for (OfferDTO offer : offers) {
			assertEquals(applicants, offer.getNumberOfApplicants());
			applicants--;
		}
	}

	@Test
	public void shouldExpectedRightCandidates() throws NotFoundException {
		OfferDTO offer = getOneOffer();
		List<CandidateDTO> candidates = candidateService
				.findAll(PageRequest.of(0, 3, Sort.by(Direction.ASC, "firstName")));

		for (CandidateDTO candidateDTO : candidates) {
			applicationService.applyToOffer(offer.getId(), candidateDTO.getId());
		}
		List<ApplicationDTO> applicants = applicationService.findAllApplicantsByOfferId(offer.getId(),
				PageRequest.of(0, 5, Sort.by(Direction.ASC, "candidate.firstName")));
		for (int i = 0; i < applicants.size(); i++) {
			assertEquals(applicants.get(i).getCandidate().getId(), candidates.get(i).getId());
		}
	}

	@Test
	public void shouldExpectedStatusToChange() throws NotFoundException, InterruptedException {
		OfferDTO offerDTO = getOneOffer();
		applicationService.applyToOffer(offerDTO.getId(), getOneCandidate().getId());
		List<ApplicationDTO> applications = applicationService.findAllApplicantsByOfferId(offerDTO.getId(),
				PageRequest.of(0, 1));
		assertEquals(ApplicationStatus.APPLIED, applications.get(0).getStatus());

		applications = applicationService.findAllApplicantsByOfferId(offerDTO.getId(), PageRequest.of(0, 1));
		applicationService.updateStatus(applications.get(0).getId(), ApplicationStatus.HIRED.toString());

		applications = applicationService.findAllApplicantsByOfferId(offerDTO.getId(), PageRequest.of(0, 1));
		assertEquals(ApplicationStatus.HIRED, applications.get(0).getStatus());
	}

	@Test
	public void shouldExpectExceptionForNonExistingOfferIdWhileOnApply() {
		assertThrows(NotFoundException.class, () -> applicationService.applyToOffer(99L, getOneCandidate().getId()));
	}

	@Test
	public void shouldExpectExceptionForCandidateApplyingTwice() throws NotFoundException {
		applicationService.applyToOffer(getOneOffer().getId(), getOneCandidate().getId());
		assertThrows(DataIntegrityViolationException.class, () -> applicationService.applyToOffer(getOneOffer().getId(), getOneCandidate().getId()));
	}
	@Test
	public void shouldExpectExceptionForDuplicateJobTitle() {
		OfferDTO generateRandomOfferDTO = GenerationUtils.generateRandomOfferDTO();
		offerService.save(generateRandomOfferDTO);
		
		//newly generated but duplicated job title
		OfferDTO generateRandomOfferDTO2 = GenerationUtils.generateRandomOfferDTO();
		generateRandomOfferDTO2.setJobTitle(generateRandomOfferDTO.getJobTitle());
		assertThrows(DataIntegrityViolationException.class, () -> offerService.save(generateRandomOfferDTO2));
	}
	
	@Test
	public void shouldExpectExceptionForNonExistingCandidateIdOnApply() {
		assertThrows(NotFoundException.class, () -> applicationService.applyToOffer(getOneOffer().getId(), 99L));
	}
	
	@Test
	public void shouldExpectExceptionForNonExistingCandidateIdAndOfferIDOnApply() {
		assertThrows(NotFoundException.class, () -> applicationService.applyToOffer(99L, 99L));
	}

	@Test
	public void shouldExpectExceptionForNonExistingStatusOnStatusChange() throws NotFoundException {
		OfferDTO offerDTO = getOneOffer();
		ApplicationDTO application = applicationService.applyToOffer(offerDTO.getId(), getOneCandidate().getId());

		assertThrows(IllegalArgumentException.class,
				() -> applicationService.updateStatus(application.getId(), "NON-EXISTING"));
	}
	@Test
	public void shouldExpectExceptionForNonExistingApplicationOnStatusChange() throws NotFoundException {
		assertThrows(NotFoundException.class, () -> applicationService.updateStatus(99L, "HIRED"));
	}

	private CandidateDTO getOneCandidate() {
		return candidateService.findAll(PageRequest.of(0, 1)).get(0);
	}

	private OfferDTO getOneOffer() {
		return offerService.findAll(PageRequest.of(0, 1)).get(0);
	}

}
