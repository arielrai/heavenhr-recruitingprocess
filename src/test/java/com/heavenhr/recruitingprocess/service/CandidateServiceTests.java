package com.heavenhr.recruitingprocess.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.heavenhr.recruitingprocess.dto.CandidateDTO;
import com.heavenhr.recruitingprocess.repository.CandidateRepository;
import com.heavenhr.recruitingprocess.utils.GenerationUtils;

import javassist.NotFoundException;

@SpringBootTest
public class CandidateServiceTests {

	@Autowired 
	private CandidateRepository candidateRepository;
	@Autowired 
	private CandidateService candidateService;

	/**
	 * Cleans up the database for each test
	 */
	@AfterEach
	public void cleanUp() {
		candidateRepository.deleteAll();
	}
	
	@Test
	public void shouldExpectSameAmountOfInsertedCandidates() {
		candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		
		List<CandidateDTO> allCandidates = candidateService.findAll(Pageable.unpaged());
		assertEquals(2, allCandidates.size());
	}
	
	@Test
	public void shouldExpectSameDataOfInsertedCandidate() throws NotFoundException {
		CandidateDTO generateRandomCandidateDTO = GenerationUtils.generateRandomCandidateDTO();
		generateRandomCandidateDTO = candidateService.save(generateRandomCandidateDTO);
		
		assertEquals(candidateService.findOne(generateRandomCandidateDTO.getId()), generateRandomCandidateDTO);
	}
	
	@Test
	public void shouldExpectOnlyOneFromPaginatedResult() {
		candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		
		List<CandidateDTO> allCandidates = candidateService.findAll(PageRequest.of(0, 1));
		assertEquals(1, allCandidates.size());
	}
	
	@Test
	public void shouldExpectExceptionForNonExistingCandidateId() {
		assertThrows(NotFoundException.class, () ->candidateService.findOne(99L));
	}

}
