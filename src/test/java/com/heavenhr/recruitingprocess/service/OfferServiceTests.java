package com.heavenhr.recruitingprocess.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.heavenhr.recruitingprocess.dto.OfferDTO;
import com.heavenhr.recruitingprocess.repository.OfferRepository;
import com.heavenhr.recruitingprocess.utils.GenerationUtils;

import javassist.NotFoundException;

@SpringBootTest
public class OfferServiceTests {
	
	@Autowired 
	private OfferRepository offerRepository;
	@Autowired 
	private OfferService offerService;

	/**
	 * Cleans up the database for each test
	 */
	@AfterEach
	public void cleanUp() {
		offerRepository.deleteAll();
	}
	
	@Test
	public void shouldExpectSameAmountOfInsertedOffers() {
		offerService.save(GenerationUtils.generateRandomOfferDTO());
		offerService.save(GenerationUtils.generateRandomOfferDTO());
		
		List<OfferDTO> allOffers = offerService.findAll(Pageable.unpaged());
		assertEquals(2, allOffers.size());
	}
	
	@Test
	public void shouldExpectSameDataOfInsertedOffer() throws NotFoundException {
		OfferDTO generateRandomOfferDTO = GenerationUtils.generateRandomOfferDTO();
		generateRandomOfferDTO = offerService.save(generateRandomOfferDTO);
		
		assertEquals(offerService.findOne(generateRandomOfferDTO.getId()), generateRandomOfferDTO);
	}
	
	@Test
	public void shouldExpectOnlyOneFromPaginatedResult() {
		offerService.save(GenerationUtils.generateRandomOfferDTO());
		offerService.save(GenerationUtils.generateRandomOfferDTO());
		
		List<OfferDTO> allOffers = offerService.findAll(PageRequest.of(0, 1));
		assertEquals(1, allOffers.size());
	}

	@Test
	public void shouldExpectExceptionForNonExistingOfferId() {
		assertThrows(NotFoundException.class, () -> offerService.findOne(99L));
	}
}
