package com.heavenhr.recruitingprocess.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;
import com.heavenhr.recruitingprocess.dto.CandidateDTO;
import com.heavenhr.recruitingprocess.dto.OfferDTO;
import com.heavenhr.recruitingprocess.repository.ApplicationRepository;
import com.heavenhr.recruitingprocess.repository.CandidateRepository;
import com.heavenhr.recruitingprocess.repository.OfferRepository;
import com.heavenhr.recruitingprocess.utils.RequestUtils;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired 
	private OfferRepository offerRepository;
	@Autowired 
	private CandidateRepository candidateRepository;
	@Autowired 
	private ApplicationRepository applicationRepository;

	/**
	 * Cleans up the database for each test
	 */
	@AfterEach
	public void cleanUp() {
		offerRepository.deleteAll();
		candidateRepository.deleteAll();
		applicationRepository.deleteAll();
	}
	
	@Test
	public void testCompleteFlowHiring() {
		ApplicationDTO[] applications = executeCompleteFlow();
		//After the complete flow the recruiter decided to hired the candidate
		assertEquals(HttpStatus.OK, restTemplate.postForEntity(
				String.format("/applications/%s/changeStatus/HIRED", applications[0].getId()),"", Object.class).getStatusCode());
	}
	
	@Test
	public void testCompleteFlowRejecting() {
		ApplicationDTO[] applications = executeCompleteFlow();
		//After the complete flow the recruiter decided to hired the candidate
		assertEquals(HttpStatus.OK, restTemplate.postForEntity(
				String.format("/applications/%s/changeStatus/REJECTED", applications[0].getId()),"", Object.class).getStatusCode());
	}

	private ApplicationDTO[] executeCompleteFlow() {
		//Create candidate
		String json = "{\n" + 
				"\"firstName\":\"Ariel\",\n" + 
				"\"lastName\":\"Rodrigues\",\n" + 
				"\"email\":\"ariel@gmail.com\",\n" + 
				"\"resumeText\":\"CV\"\n" + 
				"}";
		ResponseEntity<URI> responseCandidate = restTemplate.postForEntity("/candidates", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		assertEquals(HttpStatus.CREATED, responseCandidate.getStatusCode());
		
		ResponseEntity<CandidateDTO> candidateResponseEntity = restTemplate.getForEntity(responseCandidate.getBody(), CandidateDTO.class);
		
		//Create offer
		json = "{\n" + 
				"	\"jobTitle\" : \"lead developer 3\",\n" + 
				"	\"startDate\" : \"2019-10-01\"\n" + 
				"}";
		ResponseEntity<URI> responseOffer = restTemplate.postForEntity("/offers", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		assertEquals(HttpStatus.CREATED, responseOffer.getStatusCode());
		
		//Check if it's possible to find the just inserted offer
		ResponseEntity<OfferDTO> offerResponseEntity = restTemplate.getForEntity(responseOffer.getBody(), OfferDTO.class);
		
		//Candidate applies to offer
		CandidateDTO candidate = candidateResponseEntity.getBody();
		OfferDTO offer = offerResponseEntity.getBody();
		ResponseEntity<ApplicationDTO> applicationResponseEntity = 
				restTemplate.postForEntity(String.format("/offers/%s/apply/%s", 
						offer.getId(), candidate.getId()),
						"", ApplicationDTO.class);
		assertEquals(HttpStatus.CREATED, applicationResponseEntity.getStatusCode());
		
		//Recruiter list applicants
		ApplicationDTO[] applications = restTemplate.getForObject(String.format("/offers/%s/applications", offer.getId()), ApplicationDTO[].class);
		//Recruiter liked the first candidate and invite him over, but the recruiter misstyped INVITED
		ResponseEntity<String> applicationHiret = restTemplate.postForEntity(String.format("/applications/%s/changeStatus/INVITET", applications[0].getId()),"", String.class);
		assertEquals(HttpStatus.BAD_REQUEST, applicationHiret.getStatusCode());
		//Now the recruiter typed it properly
		ResponseEntity<Object> applicationStatusChange = restTemplate.postForEntity(String.format("/applications/%s/changeStatus/INVITED", applications[0].getId()),"", Object.class);
		assertEquals(HttpStatus.OK, applicationStatusChange.getStatusCode());
		return applications;
	}
	
}
