package com.heavenhr.recruitingprocess.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.heavenhr.recruitingprocess.repository.CandidateRepository;
import com.heavenhr.recruitingprocess.service.CandidateService;
import com.heavenhr.recruitingprocess.utils.GenerationUtils;
import com.heavenhr.recruitingprocess.utils.RequestUtils;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CandidateControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired 
	private CandidateRepository candidateRepository;
	
	@Autowired 
	private CandidateService candidateService;

	/**
	 * Cleans up the database for each test
	 */
	@AfterEach
	public void cleanUp() {
		candidateRepository.deleteAll();
	}
	
	@Test
	public void shouldCreateAndFindSameJson() throws Exception {
		String json = "{\n" + 
				"\"firstName\":\"Ariel\",\n" + 
				"\"lastName\":\"Rodrigues\",\n" + 
				"\"email\":\"ariel@gmail.com\",\n" + 
				"\"resumeText\":\"CV\"\n" + 
				"}";
		ResponseEntity<URI> response = restTemplate.postForEntity("/candidates", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		
		//Check if it's possible to find the just inserted candidate
		ResponseEntity<String> candidateResponseEntity = restTemplate.getForEntity(response.getBody(), String.class);
		assertEquals(HttpStatus.OK, candidateResponseEntity.getStatusCode());
		JSONAssert.assertEquals(json, candidateResponseEntity.getBody(), JSONCompareMode.LENIENT);
	}
	
	@Test
	public void shouldReturn404OnNonExistingId() throws Exception {
		ResponseEntity<String> reponseEntity = restTemplate.getForEntity("/candidates/99", String.class);
		assertEquals(HttpStatus.NOT_FOUND, reponseEntity.getStatusCode());
	}
	
	@Test
	public void shouldListRightNumberOfCandidades() throws Exception {
		candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		
		ResponseEntity<List> responseEntity = restTemplate.getForEntity("/candidates", List.class);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(3, responseEntity.getBody().size());
	}
	
}
