package com.heavenhr.recruitingprocess.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.heavenhr.recruitingprocess.dto.ApplicationDTO;
import com.heavenhr.recruitingprocess.dto.CandidateDTO;
import com.heavenhr.recruitingprocess.dto.OfferDTO;
import com.heavenhr.recruitingprocess.model.ApplicationStatus;
import com.heavenhr.recruitingprocess.repository.ApplicationRepository;
import com.heavenhr.recruitingprocess.repository.CandidateRepository;
import com.heavenhr.recruitingprocess.repository.OfferRepository;
import com.heavenhr.recruitingprocess.service.CandidateService;
import com.heavenhr.recruitingprocess.service.OfferService;
import com.heavenhr.recruitingprocess.utils.GenerationUtils;
import com.heavenhr.recruitingprocess.utils.RequestUtils;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OfferControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired 
	private OfferRepository offerRepository;
	@Autowired 
	private OfferService offerService;
	@Autowired 
	private CandidateRepository candidateRepository;
	@Autowired 
	private CandidateService candidateService;
	@Autowired 
	private ApplicationRepository applicationRepository;

	/**
	 * Cleans up the database for each test
	 */
	@AfterEach
	public void cleanUp() {
		offerRepository.deleteAll();
		candidateRepository.deleteAll();
		applicationRepository.deleteAll();
	}
	
	@Test
	public void shouldCreateAndFindSameJson() throws Exception {
		String json = "{\n" + 
				"	\"jobTitle\" : \"lead developer 3\",\n" + 
				"	\"startDate\" : \"2019-10-01\"\n" + 
				"}";
		ResponseEntity<URI> response = restTemplate.postForEntity("/offers", 
				RequestUtils.buildJsonRequest(json),
				URI.class);	
		
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		
		//Check if it's possible to find the just inserted offer
		ResponseEntity<String> offerResponseEntity = restTemplate.getForEntity(response.getBody(), String.class);
		assertEquals(HttpStatus.OK, offerResponseEntity.getStatusCode());
		JSONAssert.assertEquals(json, offerResponseEntity.getBody(), JSONCompareMode.LENIENT);
	}
	
	@Test
	public void shouldReturn404OnNonExistingId() throws Exception {
		ResponseEntity<String> reponseEntity = restTemplate.getForEntity("/offers/99", String.class);
		assertEquals(HttpStatus.NOT_FOUND, reponseEntity.getStatusCode());
	}
	
	@Test
	public void shouldListRightNumberOfCandidades() throws Exception {
		offerService.save(GenerationUtils.generateRandomOfferDTO());
		offerService.save(GenerationUtils.generateRandomOfferDTO());
		offerService.save(GenerationUtils.generateRandomOfferDTO());
		
		ResponseEntity<List> responseEntity = restTemplate.getForEntity("/offers", List.class);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(3, responseEntity.getBody().size());
	}
	
	@Test
	public void shouldExpectApplication() throws Exception {
		OfferDTO offer = offerService.save(GenerationUtils.generateRandomOfferDTO());
		CandidateDTO candidate = candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		
		ResponseEntity<ApplicationDTO> reponseEntity = 
				restTemplate.postForEntity(String.format("/offers/%s/apply/%s", offer.getId(), candidate.getId()),"", ApplicationDTO.class);
		assertEquals(HttpStatus.CREATED, reponseEntity.getStatusCode());
		
		ApplicationDTO applicationDTO = reponseEntity.getBody();
		assertEquals(ApplicationStatus.APPLIED, applicationDTO.getStatus());
		assertEquals(offer, applicationDTO.getOffer());
		assertEquals(candidate, applicationDTO.getCandidate());
	}
	
	@Test
	public void shouldExpectRightApplicant() throws Exception {
		OfferDTO offer = offerService.save(GenerationUtils.generateRandomOfferDTO());
		CandidateDTO candidate = candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		CandidateDTO candidate2 = candidateService.save(GenerationUtils.generateRandomCandidateDTO());
		
		ResponseEntity<ApplicationDTO> reponseEntity = 
				restTemplate.postForEntity(String.format("/offers/%s/apply/%s", offer.getId(), candidate.getId()),"", ApplicationDTO.class);
		assertEquals(HttpStatus.CREATED, reponseEntity.getStatusCode());
		
		reponseEntity = 
				restTemplate.postForEntity(String.format("/offers/%s/apply/%s", offer.getId(), candidate2.getId()),"", ApplicationDTO.class);
		assertEquals(HttpStatus.CREATED, reponseEntity.getStatusCode());
		
		
		ApplicationDTO[] applications = restTemplate.getForObject(String.format("/offers/%s/applications", offer.getId()), ApplicationDTO[].class);
		
		assertEquals(2, applications.length);
		assertEquals(candidate, applications[0].getCandidate());
		assertEquals(candidate2, applications[1].getCandidate());
		
	}
}
